<?php
/*
 * 2014-10-21
 * @author Prawee Wongsa <konkeanweb@gmail.com>
 * http://startbootstrap.com/templates/grayscale
 */
namespace prawee\theme\grayscale;

use yii\web\AssetBundle;
class GrayscaleAsset extends AssetBundle
{
    public $sourcePath='@vendor/prawee/yii2-theme-grayscale/assets';
    public $baseUrl = '@web';
    
    public $css=[
        'css/grayscale.css',
        'font-awesome-4.1.0/css/font-awesome.min.css',
    ];
    
    public $js=[
        'js/grayscale.js',
        'js/jquery-1.11.0.js',
        'js/jquery.easing.min.js',
    ];
    
    public $depends = [
        //'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
    
    public function init() {
        parent::init();
    }
}
